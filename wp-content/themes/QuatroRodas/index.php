<?php get_header(); ?>

<?php 
	if (have_posts()) {
		$postsContent = array();
		$count = 0;
		while (have_posts()) {
			the_post();	
			$postsContent[$count]['title'] = get_the_title();
			$postsContent[$count]['content'] = get_the_content();
			$postsContent[$count]['category'] = get_the_category();

			$count += 1;
		}

		$postsContent = array_reverse($postsContent);
	} 
?>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding post">
						<?= $postsContent[0]['content'] ?>
						<div class="description-post">
							<p class="category "><?= $postsContent[0]['category'][0]->name ?></p>
							<h1 class="title text-right"><?= $postsContent[0]['title'] ?></h1>
							<div class="opacity"></div>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 nopadding post">
						<?= $postsContent[1]['content'] ?>
						<div class="description-post">
							<p class="category text-right"><?= $postsContent[1]['category'][0]->name ?></p>
							<h1 class="title text-right"><?= $postsContent[1]['title'] ?></h1>
							<div class="opacity"></div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 nopadding post">
						<img src="wp-content/uploads/2016/06/img_1.jpeg" class="img-responsive" />
						<div class="description-post">
							
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 nopadding post">
						<?= $postsContent[2]['content'] ?>
						<div class="description-post">
							<p class="category text-right"><?= $postsContent[2]['category'][0]->name ?></p>
							<h1 class="title text-right"><?= $postsContent[2]['title'] ?></h1>
							<div class="opacity"></div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 nopadding post">
						<?= $postsContent[3]['content'] ?>
						<div class="description-post">
							<p class="category text-right"><?= $postsContent[3]['category'][0]->name ?></p>
							<h1 class="title text-right"><?= $postsContent[3]['title'] ?></h1>
							<div class="opacity"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="row preview-bottom">
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
					<?= $postsContent[4]['content'] ?>
					<h1><?= $postsContent[4]['title'] ?></h1>

				</div>

				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
					<?= $postsContent[5]['content'] ?>
					<h1><?= $postsContent[5]['title'] ?></h1>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
					<?= $postsContent[6]['content'] ?>
					<h1><?= $postsContent[6]['title'] ?></h1>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
					<?= $postsContent[7]['content'] ?>
					<h1><?= $postsContent[7]['title'] ?></h1>
				</div>
			</div>


		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
