<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

	<div class="advertiser hidden-xs"><img src="<?=get_bloginfo('template_url');?>/img/advertiser.png" /></div>
	
	<nav class="navbar navbar-inverse top">
	  	<div class="container-fluid">
		    <div class="navbar-header">
		    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	              	<span class="sr-only">Toggle navigation</span>
	              	<span class="icon-bar"></span>
	              	<span class="icon-bar"></span>
	              	<span class="icon-bar"></span>
	            </button>
		    	<a class="navbar-brand" href="#">
		    		<img src="<?=get_bloginfo('template_url');?>/img/logo.png" />
		    	</a>
		    </div>
		    <div id="navbar" class="navbar-collapse collapse">
			    <ul class="nav navbar-nav">
			     	
			     	<li class="dropdown active">
				        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				        	<strong>Carros</strong>
				        	<span class="caret"></span>
				        </a>
				        <ul class="dropdown-menu">
					        <li><a href="#">Menu 1</a></li>
					        <li><a href="#">Menu 2</a></li>
					        <li><a href="#">Menu 3</a></li> 
				        </ul>
			      	</li>

			      	<li class="dropdown">
				        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				        	<strong>Testes</strong>
				        	<span class="caret"></span>
				        </a>
				        <ul class="dropdown-menu">
					        <li><a href="#">Menu 1</a></li>
					        <li><a href="#">Menu 2</a></li>
					        <li><a href="#">Menu 3</a></li> 
				        </ul>
			      	</li>

			      	<li class="dropdown">
				        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				        	<strong>Notícias</strong>
				        	<span class="caret"></span>
				        </a>
				        <ul class="dropdown-menu">
					        <li><a href="#">Menu 1</a></li>
					        <li><a href="#">Menu 2</a></li>
					        <li><a href="#">Menu 3</a></li> 
				        </ul>
			      	</li>

			      	<li class="dropdown">
				        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				        	<strong>Auto-Serviço</strong>
				        	<span class="caret"></span>
				        </a>
				        <ul class="dropdown-menu">
					        <li><a href="#">Menu 1</a></li>
					        <li><a href="#">Menu 2</a></li>
					        <li><a href="#">Menu 3</a></li> 
				        </ul>
			      	</li>

			      	<li class="dropdown">
				        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				        	Guia de compras
				        	<span class="caret"></span>
				        </a>
				        <ul class="dropdown-menu">
					        <li><a href="#">Menu 1</a></li>
					        <li><a href="#">Menu 2</a></li>
					        <li><a href="#">Menu 3</a></li> 
				        </ul>
			      	</li>

			      	<li class="dropdown">
				        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
				        	<strong>Tabela Fipe</strong>
				        	<span class="caret"></span>
				        </a>
				        <ul class="dropdown-menu">
					        <li><a href="#">Menu 1</a></li>
					        <li><a href="#">Menu 2</a></li>
					        <li><a href="#">Menu 3</a></li> 
				        </ul>
			      	</li>

			      	<li><a href="#"><strong>Assine</strong></a></li> 

			      	<li><input type="text" class="input-search hidden-xs" placeholder="Pesquisar"></li> 

			    </ul>
			</div>
		</div>
	</nav>

	<nav class="navbar navbar-inverse bottom hidden-xs">
		<ul class="nav navbar-nav">
			<li class="plus-title"><a href="#">+ Acessados</a></li> 
			<li><a href="#">Salão do automóvel</a></li> 
			<li><a href="#">Renegade</a></li> 
			<li><a href="#">Novo Sandero</a></li> 
			<li><a href="#">Novo Fox</a></li> 
			<li><a href="#">Novo Ka</a></li> 
			<li><a href="#">HB 20</a></li> 
			<li><a href="#">Duster</a></li> 
			<li><a href="#">Golf</a></li> 
			<li><a href="#">Corolla</a></li> 
			<li><a href="#">Civic</a></li> 
			<li><a href="#">| A-Z |</a></li> 
		</ul>
	</nav>

	<?php //get_sidebar(); ?>
	

	<div id="content" class="container">